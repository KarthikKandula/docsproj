# Drops

## YouTube Videos  

### Programming / Lofi beats
* [Programming / Coding / Hacking music vol.16 (CONNECTION LOST)](https://www.youtube.com/watch?v=l9nh1l8ZIJQ)  
    - This channel has some really good Programming music
* [code-fi / lofi beats to code/relax to](https://www.youtube.com/watch?v=f02mOEt11OQ)
* [Midnight Coding in Chicago | LoFi Jazz Hip-Hop [Code - Relax - Study]](https://www.youtube.com/watch?v=_4kLioMoMrk)
* [Programming / Coding / Hacking music vol.24 (TEST MODE)](https://www.youtube.com/watch?v=ka4KN2KEGmI)
* 

## Markdown Guide  
[Markdown Guide Basic Syntax](https://www.markdownguide.org/basic-syntax/)

### Example Link
[Duck Duck Go](https://duckduckgo.com)